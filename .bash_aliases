alias grep='grep --color=auto'
alias wgreptests='grep --exclude-dir=venv --exclude-dir=.venv --exclude-dir=node_modules --exclude-dir=build --exclude-dir=__pycache__ --exclude-dir=.git --exclude-dir=i18n'
alias wgrep='wgreptests --exclude-dir=tests --exclude=\*.test.js'
